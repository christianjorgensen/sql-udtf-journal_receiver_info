-- Return Journal Receiver information.

-- Build instructions:

-- *> RUNSQLSTM SRCSTMF('&FP') COMMIT(*NONE) DFTRDBCOL(&FCN3) <*

create or replace function Journal_Receiver_Info (
      ReceiverLibrary                                  varchar(   10 ) -- Library containing journal receiver
    , ReceiverName                                     varchar(   10 ) -- Name of journal receiver
    )
  returns table(
      Journal_receiver_name                            varchar(   10 )
    , Journal_receiver_library_name                    varchar(   10 )
    , Journal_name                                     varchar(   10 )
    , Journal_library_name                             varchar(   10 )
    , Threshold                                        integer
    , Size                                             integer
    , Auxiliary_storage_pool_ASP                       integer
    , Number_of_journal_entries                        integer
    , Maximum_entry_specific_data_length               integer
    , Maximum_null_value_indicators                    integer
    , First_sequence_number                            integer
    , Minimize_entry_specific_data_for_data_areas      varchar(    3 )
    , Minimize_entry_specific_data_for_files           varchar(    6 )
    , Last_sequence_number                             integer
    , Status                                           varchar(    8 )
    , Receiver_size_option_MINFIXLEN                   varchar(    4 )
    , Receiver_maximums_option                         varchar(    7 )
    , Attached_timestamp                               timestamp
    , Detached_timestamp                               timestamp
    , Saved_timestamp                                  timestamp
    , Text                                             varchar(   50 )
    , Pending_transactions                             varchar(    3 )
    , Remote_journal_type                              varchar(    5 )
    , Local_journal_name                               varchar(   10 )
    , Local_journal_library_name                       varchar(   10 )
    , Local_journal_system                             varchar(    8 )
    , Local_journal_receiver_library_name              varchar(   10 )
    , Source_journal_name                              varchar(   10 )
    , Source_journal_library_name                      varchar(   10 )
    , Source_journal_system                            varchar(    8 )
    , Source_journal_receiver_library_name             varchar(   10 )
    , Redirected_journal_receiver_library              varchar(   10 )
    , Dual_journal_receiver_name                       varchar(   10 )
    , Dual_journal_receiver_library_name               varchar(   10 )
    , Previous_journal_receiver_name                   varchar(   10 )
    , Previous_journal_receiver_library_name           varchar(   10 )
    , Previous_dual_journal_receiver_name              varchar(   10 )
    , Previous_dual_journal_receiver_library_name      varchar(   10 )
    , Next_journal_receiver_name                       varchar(   10 )
    , Next_journal_receiver_library_name               varchar(   10 )
    , Next_dual_journal_receiver_name                  varchar(   10 )
    , Next_dual_journal_receiver_library_name          varchar(   10 )
    , Number_of_journal_entries_long                   bigint
    , Maximum_entry_specific_data_length_long          bigint
    , First_sequence_number_long                       bigint
    , Last_sequence_number_long                        bigint
    , ASP_device_name                                  varchar(   10 )
    , Local_journal_ASP_group_name                     varchar(   10 )
    , Source_journal_ASP_group_name                    varchar(   10 )
    , Fixed_length_data_JOB                            varchar(    3 )
    , Fixed_length_data_USR                            varchar(    3 )
    , Fixed_length_data_PGM                            varchar(    3 )
    , Fixed_length_data_PGMLIB                         varchar(    3 )
    , Fixed_length_data_SYSSEQ                         varchar(    3 )
    , Fixed_length_data_RMTADR                         varchar(    3 )
    , Fixed_length_data_THD                            varchar(    3 )
    , Fixed_length_data_LUW                            varchar(    3 )
    , Fixed_length_data_XID                            varchar(    3 )
    , Journal_entries_filtered                         varchar(    3 )
    , Number_of_programs_to_filter                     integer
    , Filter_by_program_array                          varchar( 2000 )
    , Filter_by_object                                 varchar(    3 )
    , Filter_images                                    varchar(    3 )
  )
language RPGLE
specific Journal_Receiver_Info
deterministic
returns null on null input
no sql
external name 'QGPL/JRNRCVINF(Journal_Receiver_Info)'
parameter style SQL
no final call
;
