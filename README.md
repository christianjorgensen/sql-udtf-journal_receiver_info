# SQL UDTF Journal Receiver Info #

A SQL user defined table function to return information about a journal receiver.

### What is this repository for? ###

* This SQL function will return information about the specified journal receiver.

* The following parameters must be specified:

Parameter                                   | Data type     | Description
--------------------------------------------|:--------------|:------------------------------------------
Receiver library                            | varchar(10)   |
Receiver name                               | varchar(10)   |

* The following attributes are returned:

Attribute                                   | Data type     | Description
--------------------------------------------|:--------------|:------------------------------------------
Journal receiver name                       | varchar(10)   | 
Journal receiver library name               | varchar(10)   | 
Journal name                                | varchar(10)   | 
Journal library name                        | varchar(10)   | 
Threshold                                   | integer       | 
Size                                        | integer       | 
Auxiliary storage pool ASP                  | integer       | 
Number of journal entries                   | integer       | 
Maximum entry specific data length          | integer       | 
Maximum null value indicators               | integer       | 
First sequence number                       | integer       | 
Minimize entry specific data for data areas | varchar(4)    | 
Minimize entry specific data for files      | varchar(7)    | 
Last sequence number                        | integer       | 
Status                                      | varchar(9)    | 
Receiver size option MINFIXLEN              | varchar(5)    | 
Receiver maximums option                    | varchar(8)    | 
Attached timestamp                          | timestamp     | 
Detached timestamp                          | timestamp     | 
Saved timestamp                             | timestamp     | 
Text                                        | varchar(50)   | 
Pending transactions                        | varchar(4)    | 
Remote journal type                         | varchar(6)    | 
Local journal name                          | varchar(10)   | 
Local journal library name                  | varchar(10)   | 
Local journal system                        | varchar(8)    | 
Local journal receiver library name         | varchar(10)   | 
Source journal name                         | varchar(10)   | 
Source journal library name                 | varchar(10)   | 
Source journal system                       | varchar(8)    | 
Source journal receiver library name        | varchar(10)   | 
Redirected journal receiver library         | varchar(10)   | 
Dual journal receiver name                  | varchar(10)   | 
Dual journal receiver library name          | varchar(10)   | 
Previous journal receiver name              | varchar(10)   | 
Previous journal receiver library name      | varchar(10)   | 
Previous dual journal receiver name         | varchar(10)   | 
Previous dual journal receiver library name | varchar(10)   | 
Next journal receiver name                  | varchar(10)   | 
Next journal receiver library name          | varchar(10)   | 
Next dual journal receiver name             | varchar(10)   | 
Next dual journal receiver library name     | varchar(10)   | 
Number of journal entries long              | bigint        | 
Maximum entry specific data length long     | bigint        | 
First sequence number long                  | bigint        | 
Last sequence number long                   | bigint        | 
ASP device name                             | varchar(10)   | 
Local journal ASP group name                | varchar(10)   | 
Source journal ASP group name               | varchar(10)   | 
Fixed length data JOB                       | varchar(4)    | 
Fixed length data USR                       | varchar(4)    | 
Fixed length data PGM                       | varchar(4)    | 
Fixed length data PGMLIB                    | varchar(4)    | 
Fixed length data SYSSEQ                    | varchar(4)    | 
Fixed length data RMTADR                    | varchar(4)    | 
Fixed length data THD                       | varchar(4)    | 
Fixed length data LUW                       | varchar(4)    | 
Fixed length data XID                       | varchar(4)    | 
Journal entries filtered                    | varchar(4)    | 
Number of programs to filter                | integer       | 
Filter by program array                     | varchar(2000) | Array of up to 100 programs filtered. Each element is 20 bytes, the first 10 characters contain the program name, and the second 10 characters contain the name of the program's library.  
Filter by object                            | varchar(4)    | 
Filter images                               | varchar(4)    | 

### How do I get set up? ###

* Clone this git repository to a local directory in the IFS, e.g. in your home directory.
* Compile the source using the following CL commands (objects will be placed in the QGPL library):

```

CRTRPGMOD MODULE(QGPL/JRNRCVINF) SRCSTMF('jrnrcvinf.rpgle')
CRTSRVPGM SRVPGM(QGPL/JRNRCVINF) EXPORT(*ALL) TEXT('Journal Receiver Information UDTF')
RUNSQLSTM SRCSTMF('udtf_Journal_Receiver_Info.sql') DFTRDBCOL(QGPL)

```
* Call the SQL function like the following
```
select * from table(qgpl.Journal_Receiver_Info( <library>, <journal receiver> )) x
```
* The next SQL statement will show all information about journal receivers in library QUSRSYS:
```
select jri.*
  from table(QSYS2.object_statistics( 'QUSRSYS', '*JRNRCV' )) ol
     , table(QGPL.journal_receiver_info( 'QUSRSYS', ol.objname )) jri
```


### Documentation ###

[Retrieve Journal Receiver Information (QjoRtvJrnReceiverInformation) API](http://www.ibm.com/support/knowledgecenter/ssw_ibm_i_71/apis/QJORRCVI.htm)