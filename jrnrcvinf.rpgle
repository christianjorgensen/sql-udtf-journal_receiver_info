**free
/if not defined( Copying_prototypes )
//---------------------------------------------------------------/
//                                                               /
//  Brief description of collection of procedures.               /
//                                                               /
//  Procedures:                                                  /
//                                                               /
//  Journal_Receiver_Info - return info about journal receiver.  /
//                                                               /
//  Compilation:                                                 /
//                                                               /
//*>  CRTRPGMOD MODULE(&FCN3/&FNR) SRCSTMF('&FP')              <*/
//*>  CRTSRVPGM SRVPGM(&FCN3/&FNR) EXPORT(*ALL) ACTGRP(&FCN3) -<*/
//*>            TEXT('Journal Receiver Information UDTF')      <*/
//*>  DLTMOD    MODULE(&FCN3/&FNR)                             <*/
//                                                               /
//---------------------------------------------------------------/
//  2016-06-15 : Christian Jorgensen                             /
//               Module created.                                 /
//  2016-09-19 : Christian Jorgensen                             /
//               Convert to IFS source.                          /
//  2016-11-17 : Christian Jorgensen                             /
//               Remove '*' from returned special values.        /
//               Fix handling of Filter by program array.        /
//---------------------------------------------------------------/

ctl-opt text( 'Journal Receiver Information UDTF' );
ctl-opt debug;
ctl-opt option( *Srcstmt : *NoDebugIO );
ctl-opt thread( *serialize );

ctl-opt nomain;

//----------------------------------------------------------------
// Exported procedures:

/endif

/if not defined( JRNRCVINF_prototype_copied )

// Procedure:    Journal_Receiver_Info
// Description:  Return information about journal receiver.

dcl-pr Journal_Receiver_Info extproc( *dclcase );
  // Incoming parameters
  Lib                                                varchar(   10 ) const;
  Rcv                                                varchar(   10 ) const;
  // Returned parameters
  p_Journal_receiver_name                            varchar(   10 );
  p_Journal_receiver_library_name                    varchar(   10 );
  p_Journal_name                                     varchar(   10 );
  p_Journal_library_name                             varchar(   10 );
  p_Threshold                                        int( 10 );
  p_Size                                             int( 10 );
  p_Auxiliary_storage_pool_ASP                       int( 10 );
  p_Number_of_journal_entries                        int( 10 );
  p_Maximum_entry_specific_data_length               int( 10 );
  p_Maximum_null_value_indicators                    int( 10 );
  p_First_sequence_number                            int( 10 );
  p_Minimize_entry_specific_data_for_data_areas      varchar(    3 );
  p_Minimize_entry_specific_data_for_files           varchar(    6 );
  p_Last_sequence_number                             int( 10 );
  p_Status                                           varchar(    8 );
  p_Receiver_size_option_MINFIXLEN                   varchar(    4 );
  p_Receiver_maximums_option                         varchar(    7 );
  p_Attached_timestamp                               timestamp;
  p_Detached_timestamp                               timestamp;
  p_Saved_timestamp                                  timestamp;
  p_Text                                             varchar(   50 );
  p_Pending_transactions                             varchar(    3 );
  p_Remote_journal_type                              varchar(    5 );
  p_Local_journal_name                               varchar(   10 );
  p_Local_journal_library_name                       varchar(   10 );
  p_Local_journal_system                             varchar(    8 );
  p_Local_journal_receiver_library_name              varchar(   10 );
  p_Source_journal_name                              varchar(   10 );
  p_Source_journal_library_name                      varchar(   10 );
  p_Source_journal_system                            varchar(    8 );
  p_Source_journal_receiver_library_name             varchar(   10 );
  p_Redirected_journal_receiver_library              varchar(   10 );
  p_Dual_journal_receiver_name                       varchar(   10 );
  p_Dual_journal_receiver_library_name               varchar(   10 );
  p_Previous_journal_receiver_name                   varchar(   10 );
  p_Previous_journal_receiver_library_name           varchar(   10 );
  p_Previous_dual_journal_receiver_name              varchar(   10 );
  p_Previous_dual_journal_receiver_library_name      varchar(   10 );
  p_Next_journal_receiver_name                       varchar(   10 );
  p_Next_journal_receiver_library_name               varchar(   10 );
  p_Next_dual_journal_receiver_name                  varchar(   10 );
  p_Next_dual_journal_receiver_library_name          varchar(   10 );
  p_Number_of_journal_entries_long                   int( 20 );
  p_Maximum_entry_specific_data_length_long          int( 20 );
  p_First_sequence_number_long                       int( 20 );
  p_Last_sequence_number_long                        int( 20 );
  p_ASP_device_name                                  varchar(   10 );
  p_Local_journal_ASP_group_name                     varchar(   10 );
  p_Source_journal_ASP_group_name                    varchar(   10 );
  p_Fixed_length_data_JOB                            varchar(    3 );
  p_Fixed_length_data_USR                            varchar(    3 );
  p_Fixed_length_data_PGM                            varchar(    3 );
  p_Fixed_length_data_PGMLIB                         varchar(    3 );
  p_Fixed_length_data_SYSSEQ                         varchar(    3 );
  p_Fixed_length_data_RMTADR                         varchar(    3 );
  p_Fixed_length_data_THD                            varchar(    3 );
  p_Fixed_length_data_LUW                            varchar(    3 );
  p_Fixed_length_data_XID                            varchar(    3 );
  p_Journal_entries_filtered                         varchar(    3 );
  p_Number_of_programs_to_filter                     int( 10 );
  p_Filter_by_program_array                          varchar( 2000 );
  p_Filter_by_object                                 varchar(    3 );
  p_Filter_images                                    varchar(    3 );
  // Null indicators for incoming parameters
  n_Lib                                              int( 5 ) const;
  n_Rcv                                              int( 5 ) const;
  // Null indicators for returned parameters
  n_Journal_receiver_name                            int( 5 );
  n_Journal_receiver_library_name                    int( 5 );
  n_Journal_name                                     int( 5 );
  n_Journal_library_name                             int( 5 );
  n_Threshold                                        int( 5 );
  n_Size                                             int( 5 );
  n_Auxiliary_storage_pool_ASP                       int( 5 );
  n_Number_of_journal_entries                        int( 5 );
  n_Maximum_entry_specific_data_length               int( 5 );
  n_Maximum_null_value_indicators                    int( 5 );
  n_First_sequence_number                            int( 5 );
  n_Minimize_entry_specific_data_for_data_areas      int( 5 );
  n_Minimize_entry_specific_data_for_files           int( 5 );
  n_Last_sequence_number                             int( 5 );
  n_Status                                           int( 5 );
  n_Receiver_size_option_MINFIXLEN                   int( 5 );
  n_Receiver_maximums_option                         int( 5 );
  n_Attached_timestamp                               int( 5 );
  n_Detached_timestamp                               int( 5 );
  n_Saved_timestamp                                  int( 5 );
  n_Text                                             int( 5 );
  n_Pending_transactions                             int( 5 );
  n_Remote_journal_type                              int( 5 );
  n_Local_journal_name                               int( 5 );
  n_Local_journal_library_name                       int( 5 );
  n_Local_journal_system                             int( 5 );
  n_Local_journal_receiver_library_name              int( 5 );
  n_Source_journal_name                              int( 5 );
  n_Source_journal_library_name                      int( 5 );
  n_Source_journal_system                            int( 5 );
  n_Source_journal_receiver_library_name             int( 5 );
  n_Redirected_journal_receiver_library              int( 5 );
  n_Dual_journal_receiver_name                       int( 5 );
  n_Dual_journal_receiver_library_name               int( 5 );
  n_Previous_journal_receiver_name                   int( 5 );
  n_Previous_journal_receiver_library_name           int( 5 );
  n_Previous_dual_journal_receiver_name              int( 5 );
  n_Previous_dual_journal_receiver_library_name      int( 5 );
  n_Next_journal_receiver_name                       int( 5 );
  n_Next_journal_receiver_library_name               int( 5 );
  n_Next_dual_journal_receiver_name                  int( 5 );
  n_Next_dual_journal_receiver_library_name          int( 5 );
  n_Number_of_journal_entries_long                   int( 5 );
  n_Maximum_entry_specific_data_length_long          int( 5 );
  n_First_sequence_number_long                       int( 5 );
  n_Last_sequence_number_long                        int( 5 );
  n_ASP_device_name                                  int( 5 );
  n_Local_journal_ASP_group_name                     int( 5 );
  n_Source_journal_ASP_group_name                    int( 5 );
  n_Fixed_length_data_JOB                            int( 5 );
  n_Fixed_length_data_USR                            int( 5 );
  n_Fixed_length_data_PGM                            int( 5 );
  n_Fixed_length_data_PGMLIB                         int( 5 );
  n_Fixed_length_data_SYSSEQ                         int( 5 );
  n_Fixed_length_data_RMTADR                         int( 5 );
  n_Fixed_length_data_THD                            int( 5 );
  n_Fixed_length_data_LUW                            int( 5 );
  n_Fixed_length_data_XID                            int( 5 );
  n_Journal_entries_filtered                         int( 5 );
  n_Number_of_programs_to_filter                     int( 5 );
  n_Filter_by_program_array                          int( 5 );
  n_Filter_by_object                                 int( 5 );
  n_Filter_images                                    int( 5 );
  // SQL parameters.
  Sql_State   char( 5 );
  Function    varchar( 517 ) const;
  Specific    varchar( 128 ) const;
  MsgText     varchar( 70 );
  CallType    int( 5 )       const;
end-pr;

//----------------------------------------------------------------
// Exported data:

/define JRNRCVINF_prototype_copied
/if defined( Copying_prototypes )
/eof

/endif
/endif

//----------------------------------------------------------------
// Constants:

dcl-c CALL_STARTUP    -2;
dcl-c CALL_OPEN       -1;
dcl-c CALL_FETCH       0;
dcl-c CALL_CLOSE       1;
dcl-c CALL_FINAL       2;

dcl-c PARM_NULL       -1;
dcl-c PARM_NOTNULL     0;

dcl-c DATE_AND_TIME_NULL '0000000000000';

//----------------------------------------------------------------
// Data types:

dcl-s  Filter_by_program_array_t                   char( 20 ) dim( 100 ) template;

dcl-ds RRCV0100_t template qualified;
  Bytes_returned                                   int( 10 );
  Bytes_available                                  int( 10 );
  Journal_receiver_name                            char( 10 );
  Journal_receiver_library_name                    char( 10 );
  Journal_name                                     char( 10 );
  Journal_library_name                             char( 10 );
  Threshold                                        int( 10 );
  Size                                             int( 10 );
  Auxiliary_storage_pool_ASP                       int( 10 );
  Number_of_journal_entries                        int( 10 );
  Maximum_entry_specific_data_length               int( 10 );
  Maximum_null_value_indicators                    int( 10 );
  First_sequence_number                            int( 10 );
  Minimize_entry_specific_data_for_data_areas      char(  1 );
  Minimize_entry_specific_data_for_files           char(  1 );
  Reserved1                                        char(  2 );
  Last_sequence_number                             int( 10 );
  Reserved2                                        int( 10 );
  Status                                           char(  1 );
  Receiver_size_option_MINFIXLEN                   char(  1 );
  Receiver_maximums_option                         char(  1 );
  Reserved3                                        char(  4 );
  Attached_date_and_time                           char( 13 );
  Detached_date_and_time                           char( 13 );
  Saved_date_and_time                              char( 13 );
  Text                                             char( 50 );
  Pending_transactions                             char(  1 );
  Remote_journal_type                              char(  1 );
  Local_journal_name                               char( 10 );
  Local_journal_library_name                       char( 10 );
  Local_journal_system                             char(  8 );
  Local_journal_receiver_library_name              char( 10 );
  Source_journal_name                              char( 10 );
  Source_journal_library_name                      char( 10 );
  Source_journal_system                            char(  8 );
  Source_journal_receiver_library_name             char( 10 );
  Redirected_journal_receiver_library              char( 10 );
  Dual_journal_receiver_name                       char( 10 );
  Dual_journal_receiver_library_name               char( 10 );
  Previous_journal_receiver_name                   char( 10 );
  Previous_journal_receiver_library_name           char( 10 );
  Previous_dual_journal_receiver_name              char( 10 );
  Previous_dual_journal_receiver_library_name      char( 10 );
  Next_journal_receiver_name                       char( 10 );
  Next_journal_receiver_library_name               char( 10 );
  Next_dual_journal_receiver_name                  char( 10 );
  Next_dual_journal_receiver_library_name          char( 10 );
  Number_of_journal_entries_long                   char( 20 );
  Maximum_entry_specific_data_length_long          char( 20 );
  First_sequence_number_long                       char( 20 );
  Last_sequence_number_long                        char( 20 );
  ASP_device_name                                  char( 10 );
  Local_journal_ASP_group_name                     char( 10 );
  Source_journal_ASP_group_name                    char( 10 );
  Fixed_length_data_JOB                            char(  1 );
  Fixed_length_data_USR                            char(  1 );
  Fixed_length_data_PGM                            char(  1 );
  Fixed_length_data_PGMLIB                         char(  1 );
  Fixed_length_data_SYSSEQ                         char(  1 );
  Fixed_length_data_RMTADR                         char(  1 );
  Fixed_length_data_THD                            char(  1 );
  Fixed_length_data_LUW                            char(  1 );
  Fixed_length_data_XID                            char(  1 );
  Journal_entries_filtered                         char(  1 );
  Offset_to_filter_by_program_array                int( 10 );
  Number_of_programs_to_filter                     int( 10 );
  Filter_by_object                                 char(  1 );
  Filter_images                                    char(  1 );
  Reserved4                                        char( 10 );
  Filter_by_program_array                          like( Filter_by_program_array_t );
end-ds;

//----------------------------------------------------------------
// Global data:

// API error data structure:

dcl-ds ApiError;
  AeBytPrv  int( 10 ) inz( %size( ApiError ) );
  AeBytAvl  int( 10 );
  AeExcpId  char(   7 );
  *n        char(   1 );
  AeExcpDta char( 128 );
end-ds;

//----------------------------------------------------------------
// Prototypes:

//----------------------------------------------------------------
// Procedure:    Journal_Receiver_Info
// Description:  Return information about journal receiver.

dcl-proc Journal_Receiver_Info export;

  dcl-pi *n;
    // Incoming parameters
    Lib                                                varchar(   10 ) const;
    Rcv                                                varchar(   10 ) const;
    // Returned parameters
    p_Journal_receiver_name                            varchar(   10 );
    p_Journal_receiver_library_name                    varchar(   10 );
    p_Journal_name                                     varchar(   10 );
    p_Journal_library_name                             varchar(   10 );
    p_Threshold                                        int( 10 );
    p_Size                                             int( 10 );
    p_Auxiliary_storage_pool_ASP                       int( 10 );
    p_Number_of_journal_entries                        int( 10 );
    p_Maximum_entry_specific_data_length               int( 10 );
    p_Maximum_null_value_indicators                    int( 10 );
    p_First_sequence_number                            int( 10 );
    p_Minimize_entry_specific_data_for_data_areas      varchar(    3 );
    p_Minimize_entry_specific_data_for_files           varchar(    6 );
    p_Last_sequence_number                             int( 10 );
    p_Status                                           varchar(    8 );
    p_Receiver_size_option_MINFIXLEN                   varchar(    4 );
    p_Receiver_maximums_option                         varchar(    7 );
    p_Attached_timestamp                               timestamp;
    p_Detached_timestamp                               timestamp;
    p_Saved_timestamp                                  timestamp;
    p_Text                                             varchar(   50 );
    p_Pending_transactions                             varchar(    3 );
    p_Remote_journal_type                              varchar(    5 );
    p_Local_journal_name                               varchar(   10 );
    p_Local_journal_library_name                       varchar(   10 );
    p_Local_journal_system                             varchar(    8 );
    p_Local_journal_receiver_library_name              varchar(   10 );
    p_Source_journal_name                              varchar(   10 );
    p_Source_journal_library_name                      varchar(   10 );
    p_Source_journal_system                            varchar(    8 );
    p_Source_journal_receiver_library_name             varchar(   10 );
    p_Redirected_journal_receiver_library              varchar(   10 );
    p_Dual_journal_receiver_name                       varchar(   10 );
    p_Dual_journal_receiver_library_name               varchar(   10 );
    p_Previous_journal_receiver_name                   varchar(   10 );
    p_Previous_journal_receiver_library_name           varchar(   10 );
    p_Previous_dual_journal_receiver_name              varchar(   10 );
    p_Previous_dual_journal_receiver_library_name      varchar(   10 );
    p_Next_journal_receiver_name                       varchar(   10 );
    p_Next_journal_receiver_library_name               varchar(   10 );
    p_Next_dual_journal_receiver_name                  varchar(   10 );
    p_Next_dual_journal_receiver_library_name          varchar(   10 );
    p_Number_of_journal_entries_long                   int( 20 );
    p_Maximum_entry_specific_data_length_long          int( 20 );
    p_First_sequence_number_long                       int( 20 );
    p_Last_sequence_number_long                        int( 20 );
    p_ASP_device_name                                  varchar(   10 );
    p_Local_journal_ASP_group_name                     varchar(   10 );
    p_Source_journal_ASP_group_name                    varchar(   10 );
    p_Fixed_length_data_JOB                            varchar(    3 );
    p_Fixed_length_data_USR                            varchar(    3 );
    p_Fixed_length_data_PGM                            varchar(    3 );
    p_Fixed_length_data_PGMLIB                         varchar(    3 );
    p_Fixed_length_data_SYSSEQ                         varchar(    3 );
    p_Fixed_length_data_RMTADR                         varchar(    3 );
    p_Fixed_length_data_THD                            varchar(    3 );
    p_Fixed_length_data_LUW                            varchar(    3 );
    p_Fixed_length_data_XID                            varchar(    3 );
    p_Journal_entries_filtered                         varchar(    3 );
    p_Number_of_programs_to_filter                     int( 10 );
    p_Filter_by_program_array                          varchar( 2000 );
    p_Filter_by_object                                 varchar(    3 );
    p_Filter_images                                    varchar(    3 );
    // Null indicators for incoming parameters
    n_Lib                                              int( 5 ) const;
    n_Rcv                                              int( 5 ) const;
    // Null indicators for returned parameters
    n_Journal_receiver_name                            int( 5 );
    n_Journal_receiver_library_name                    int( 5 );
    n_Journal_name                                     int( 5 );
    n_Journal_library_name                             int( 5 );
    n_Threshold                                        int( 5 );
    n_Size                                             int( 5 );
    n_Auxiliary_storage_pool_ASP                       int( 5 );
    n_Number_of_journal_entries                        int( 5 );
    n_Maximum_entry_specific_data_length               int( 5 );
    n_Maximum_null_value_indicators                    int( 5 );
    n_First_sequence_number                            int( 5 );
    n_Minimize_entry_specific_data_for_data_areas      int( 5 );
    n_Minimize_entry_specific_data_for_files           int( 5 );
    n_Last_sequence_number                             int( 5 );
    n_Status                                           int( 5 );
    n_Receiver_size_option_MINFIXLEN                   int( 5 );
    n_Receiver_maximums_option                         int( 5 );
    n_Attached_timestamp                               int( 5 );
    n_Detached_timestamp                               int( 5 );
    n_Saved_timestamp                                  int( 5 );
    n_Text                                             int( 5 );
    n_Pending_transactions                             int( 5 );
    n_Remote_journal_type                              int( 5 );
    n_Local_journal_name                               int( 5 );
    n_Local_journal_library_name                       int( 5 );
    n_Local_journal_system                             int( 5 );
    n_Local_journal_receiver_library_name              int( 5 );
    n_Source_journal_name                              int( 5 );
    n_Source_journal_library_name                      int( 5 );
    n_Source_journal_system                            int( 5 );
    n_Source_journal_receiver_library_name             int( 5 );
    n_Redirected_journal_receiver_library              int( 5 );
    n_Dual_journal_receiver_name                       int( 5 );
    n_Dual_journal_receiver_library_name               int( 5 );
    n_Previous_journal_receiver_name                   int( 5 );
    n_Previous_journal_receiver_library_name           int( 5 );
    n_Previous_dual_journal_receiver_name              int( 5 );
    n_Previous_dual_journal_receiver_library_name      int( 5 );
    n_Next_journal_receiver_name                       int( 5 );
    n_Next_journal_receiver_library_name               int( 5 );
    n_Next_dual_journal_receiver_name                  int( 5 );
    n_Next_dual_journal_receiver_library_name          int( 5 );
    n_Number_of_journal_entries_long                   int( 5 );
    n_Maximum_entry_specific_data_length_long          int( 5 );
    n_First_sequence_number_long                       int( 5 );
    n_Last_sequence_number_long                        int( 5 );
    n_ASP_device_name                                  int( 5 );
    n_Local_journal_ASP_group_name                     int( 5 );
    n_Source_journal_ASP_group_name                    int( 5 );
    n_Fixed_length_data_JOB                            int( 5 );
    n_Fixed_length_data_USR                            int( 5 );
    n_Fixed_length_data_PGM                            int( 5 );
    n_Fixed_length_data_PGMLIB                         int( 5 );
    n_Fixed_length_data_SYSSEQ                         int( 5 );
    n_Fixed_length_data_RMTADR                         int( 5 );
    n_Fixed_length_data_THD                            int( 5 );
    n_Fixed_length_data_LUW                            int( 5 );
    n_Fixed_length_data_XID                            int( 5 );
    n_Journal_entries_filtered                         int( 5 );
    n_Number_of_programs_to_filter                     int( 5 );
    n_Filter_by_program_array                          int( 5 );
    n_Filter_by_object                                 int( 5 );
    n_Filter_images                                    int( 5 );
    // SQL parameters.
    Sql_State   char( 5 );
    Function    varchar( 517 ) const;
    Specific    varchar( 128 ) const;
    MsgText     varchar( 70 );
    CallType    int( 5 )       const;
  end-pi;

  dcl-pr QjoRtvJrnReceiverInformation extproc( *dclcase );
    *n likeds( RRCV0100_t );
    *n int( 10 )    const;
    *n char(   20 ) const;
    *n char(    8 ) const;
    *n char( 1024 ) const options( *varsize );
  end-pr;

  dcl-s  LoRcv        char( 10 );
  dcl-s  LoLib        char( 10 );
  dcl-s  SaveCallType like( CallType ) static;  // Save CallType from previous call...
  dcl-s  SaveLoRcv    like( LoRcv    ) static;  // Save receiver from previous call...
  dcl-s  SaveLoLib    like( LoLib    ) static;  // Save library from previous call...

  dcl-ds RRCV0100     likeds( RRCV0100_t );

  //   Start all fields at not NULL.

  n_Journal_receiver_name                            = PARM_NOTNULL;
  n_Journal_receiver_library_name                    = PARM_NOTNULL;
  n_Journal_name                                     = PARM_NOTNULL;
  n_Journal_library_name                             = PARM_NOTNULL;
  n_Threshold                                        = PARM_NOTNULL;
  n_Size                                             = PARM_NOTNULL;
  n_Auxiliary_storage_pool_ASP                       = PARM_NOTNULL;
  n_Number_of_journal_entries                        = PARM_NOTNULL;
  n_Maximum_entry_specific_data_length               = PARM_NOTNULL;
  n_Maximum_null_value_indicators                    = PARM_NOTNULL;
  n_First_sequence_number                            = PARM_NOTNULL;
  n_Minimize_entry_specific_data_for_data_areas      = PARM_NOTNULL;
  n_Minimize_entry_specific_data_for_files           = PARM_NOTNULL;
  n_Last_sequence_number                             = PARM_NOTNULL;
  n_Status                                           = PARM_NOTNULL;
  n_Receiver_size_option_MINFIXLEN                   = PARM_NOTNULL;
  n_Receiver_maximums_option                         = PARM_NOTNULL;
  n_Attached_timestamp                               = PARM_NOTNULL;
  n_Detached_timestamp                               = PARM_NOTNULL;
  n_Saved_timestamp                                  = PARM_NOTNULL;
  n_Text                                             = PARM_NOTNULL;
  n_Pending_transactions                             = PARM_NOTNULL;
  n_Remote_journal_type                              = PARM_NOTNULL;
  n_Local_journal_name                               = PARM_NOTNULL;
  n_Local_journal_library_name                       = PARM_NOTNULL;
  n_Local_journal_system                             = PARM_NOTNULL;
  n_Local_journal_receiver_library_name              = PARM_NOTNULL;
  n_Source_journal_name                              = PARM_NOTNULL;
  n_Source_journal_library_name                      = PARM_NOTNULL;
  n_Source_journal_system                            = PARM_NOTNULL;
  n_Source_journal_receiver_library_name             = PARM_NOTNULL;
  n_Redirected_journal_receiver_library              = PARM_NOTNULL;
  n_Dual_journal_receiver_name                       = PARM_NOTNULL;
  n_Dual_journal_receiver_library_name               = PARM_NOTNULL;
  n_Previous_journal_receiver_name                   = PARM_NOTNULL;
  n_Previous_journal_receiver_library_name           = PARM_NOTNULL;
  n_Previous_dual_journal_receiver_name              = PARM_NOTNULL;
  n_Previous_dual_journal_receiver_library_name      = PARM_NOTNULL;
  n_Next_journal_receiver_name                       = PARM_NOTNULL;
  n_Next_journal_receiver_library_name               = PARM_NOTNULL;
  n_Next_dual_journal_receiver_name                  = PARM_NOTNULL;
  n_Next_dual_journal_receiver_library_name          = PARM_NOTNULL;
  n_Number_of_journal_entries_long                   = PARM_NOTNULL;
  n_Maximum_entry_specific_data_length_long          = PARM_NOTNULL;
  n_First_sequence_number_long                       = PARM_NOTNULL;
  n_Last_sequence_number_long                        = PARM_NOTNULL;
  n_ASP_device_name                                  = PARM_NOTNULL;
  n_Local_journal_ASP_group_name                     = PARM_NOTNULL;
  n_Source_journal_ASP_group_name                    = PARM_NOTNULL;
  n_Fixed_length_data_JOB                            = PARM_NOTNULL;
  n_Fixed_length_data_USR                            = PARM_NOTNULL;
  n_Fixed_length_data_PGM                            = PARM_NOTNULL;
  n_Fixed_length_data_PGMLIB                         = PARM_NOTNULL;
  n_Fixed_length_data_SYSSEQ                         = PARM_NOTNULL;
  n_Fixed_length_data_RMTADR                         = PARM_NOTNULL;
  n_Fixed_length_data_THD                            = PARM_NOTNULL;
  n_Fixed_length_data_LUW                            = PARM_NOTNULL;
  n_Fixed_length_data_XID                            = PARM_NOTNULL;
  n_Journal_entries_filtered                         = PARM_NOTNULL;
  n_Number_of_programs_to_filter                     = PARM_NOTNULL;
  n_Filter_by_program_array                          = PARM_NOTNULL;
  n_Filter_by_object                                 = PARM_NOTNULL;
  n_Filter_images                                    = PARM_NOTNULL;

  //  Open, fetch & close...

  select;
    when  CallType = CALL_FETCH;

      // Verify that library and receiver was specified.

      if ( n_Lib = PARM_NULL ) or
         ( Lib = '' );
         SQL_State = '38999';
         MsgText = 'Library must be specified';
         return;
      else;
        LoLib = Lib;
      endif;

      if ( n_Rcv = PARM_NULL ) or
         ( Rcv = '' );
          SQL_State = '38999';
          MsgText = 'Journal receiver must be specified';
          return;
      else;
        LoRcv = Rcv;
      endif;

      // If previous call was for fetch for same receiver, return EOF.

      if ( CallType = SaveCallType ) and
         ( LoLib    = SaveLoLib    ) and
         ( LoRcv    = SaveLoRcv    );
        SQL_State = '02000';
        return;
      endif;

      // Get journal receiver information.

      QjoRtvJrnReceiverInformation( RRCV0100
              : %size( RRCV0100 )
              : LoRcv + LoLib
              : 'RRCV0100'
              : ApiError
              );

      if ( AeBytAvl > 0 );
        SQL_State = '38999';
        MsgText = 'Error ' + AeExcpID + ', please check joblog.';
        return;
      endif;

      p_Journal_receiver_name                         = %trimr( RRCV0100.Journal_receiver_name         );
      p_Journal_receiver_library_name                 = %trimr( RRCV0100.Journal_receiver_library_name );
      p_Journal_name                                  = %trimr( RRCV0100.Journal_name                  );
      p_Journal_library_name                          = %trimr( RRCV0100.Journal_library_name          );
      p_Threshold                                     = RRCV0100.Threshold                              ;
      p_Size                                          = RRCV0100.Size                                   ;
      p_Auxiliary_storage_pool_ASP                    = RRCV0100.Auxiliary_storage_pool_ASP             ;
      p_Number_of_journal_entries                     = RRCV0100.Number_of_journal_entries              ;
      p_Maximum_entry_specific_data_length            = RRCV0100.Maximum_entry_specific_data_length     ;
      p_Maximum_null_value_indicators                 = RRCV0100.Maximum_null_value_indicators          ;
      p_First_sequence_number                         = RRCV0100.First_sequence_number                  ;

      select;
        when ( RRCV0100.Minimize_entry_specific_data_for_data_areas = '0' );
          p_Minimize_entry_specific_data_for_data_areas = 'NO';
        when ( RRCV0100.Minimize_entry_specific_data_for_data_areas = '1' );
          p_Minimize_entry_specific_data_for_data_areas = 'YES';
      endsl;

      select;
        when ( RRCV0100.Minimize_entry_specific_data_for_files = '0' );
          p_Minimize_entry_specific_data_for_files = 'NONE';
        when ( RRCV0100.Minimize_entry_specific_data_for_files = '1' );
          p_Minimize_entry_specific_data_for_files = 'FILE';
        when ( RRCV0100.Minimize_entry_specific_data_for_files = '2' );
          p_Minimize_entry_specific_data_for_files = 'FLDBDY';
      endsl;

      p_Last_sequence_number                          = RRCV0100.Last_sequence_number                        ;

      select;
        when ( RRCV0100.Status = '1' );
          p_Status = 'ATTACHED';
        when ( RRCV0100.Status = '2' );
          p_Status = 'ONLINE';
        when ( RRCV0100.Status = '3' );
          p_Status = 'SAVED';
        when ( RRCV0100.Status = '4' );
          p_Status = 'FREED';
        when ( RRCV0100.Status = '5' );
          p_Status = 'PARTIAL';
        when ( RRCV0100.Status = '6' );
          p_Status = 'EMPTY';
      endsl;

      select;
        when ( RRCV0100.Receiver_size_option_MINFIXLEN = ' ' );
          n_Receiver_size_option_MINFIXLEN = PARM_NULL;
        when ( RRCV0100.Receiver_size_option_MINFIXLEN = '0' );
          p_Receiver_size_option_MINFIXLEN = 'ALL';
        when ( RRCV0100.Receiver_size_option_MINFIXLEN = '1' );
          p_Receiver_size_option_MINFIXLEN = 'NONE';
      endsl;

      select;
        when ( RRCV0100.Receiver_maximums_option = ' ' );
          n_Receiver_maximums_option = PARM_NULL;
        when ( RRCV0100.Receiver_maximums_option = '0' );
          p_Receiver_maximums_option = 'NONE';
        when ( RRCV0100.Receiver_maximums_option = '1' );
          p_Receiver_maximums_option = 'MAXOPT1';
        when ( RRCV0100.Receiver_maximums_option = '2' );
          p_Receiver_maximums_option = 'MAXOPT2';
        when ( RRCV0100.Receiver_maximums_option = '3' );
          p_Receiver_maximums_option = 'MAXOPT3';
      endsl;

      if ( RRCV0100.Attached_date_and_time <> DATE_AND_TIME_NULL );
        p_Attached_timestamp                          = CvtTimestamp( RRCV0100.Attached_date_and_time );
      else;
        n_Attached_timestamp                          = PARM_NULL;
      endif;

      if ( RRCV0100.Detached_date_and_time <> DATE_AND_TIME_NULL );
        p_Detached_timestamp                          = CvtTimestamp( RRCV0100.Detached_date_and_time );
      else;
        n_Detached_timestamp                          = PARM_NULL;
      endif;

      if ( RRCV0100.Saved_date_and_time <> DATE_AND_TIME_NULL );
        p_Saved_timestamp                             = CvtTimestamp( RRCV0100.Saved_date_and_time    );
      else;
        n_Saved_timestamp                             = PARM_NULL;
      endif;

      p_Text                                          = %trimr( RRCV0100.Text );

      select;
        when ( RRCV0100.Pending_transactions = '0' );
          p_Pending_transactions = 'NO';
        when ( RRCV0100.Pending_transactions = '1' );
          p_Pending_transactions = 'YES';
      endsl;

      select;
        when ( RRCV0100.Remote_journal_type = ' ' );
          n_Remote_journal_type = PARM_NULL;
        when ( RRCV0100.Remote_journal_type = '0' );
          p_Remote_journal_type = 'LOCAL';
        when ( RRCV0100.Remote_journal_type = '1' );
          p_Remote_journal_type = 'TYPE1';
        when ( RRCV0100.Remote_journal_type = '2' );
          p_Remote_journal_type = 'TYPE2';
      endsl;

      p_Local_journal_name                            = %trimr( RRCV0100.Local_journal_name                          );
      p_Local_journal_library_name                    = %trimr( RRCV0100.Local_journal_library_name                  );
      p_Local_journal_system                          = %trimr( RRCV0100.Local_journal_system                        );
      p_Local_journal_receiver_library_name           = %trimr( RRCV0100.Local_journal_receiver_library_name         );
      p_Source_journal_name                           = %trimr( RRCV0100.Source_journal_name                         );
      p_Source_journal_library_name                   = %trimr( RRCV0100.Source_journal_library_name                 );
      p_Source_journal_system                         = %trimr( RRCV0100.Source_journal_system                       );
      p_Source_journal_receiver_library_name          = %trimr( RRCV0100.Source_journal_receiver_library_name        );

      // Only use value if different than all x'00' - error in API (description)!
      if ( RRCV0100.Redirected_journal_receiver_library <> *allx'00' );
        p_Redirected_journal_receiver_library         = %trimr( RRCV0100.Redirected_journal_receiver_library         );
      else;
        p_Redirected_journal_receiver_library         = '';
      endif;

      p_Dual_journal_receiver_name                    = %trimr( RRCV0100.Dual_journal_receiver_name                  );
      p_Dual_journal_receiver_library_name            = %trimr( RRCV0100.Dual_journal_receiver_library_name          );
      p_Previous_journal_receiver_name                = %trimr( RRCV0100.Previous_journal_receiver_name              );
      p_Previous_journal_receiver_library_name        = %trimr( RRCV0100.Previous_journal_receiver_library_name      );
      p_Previous_dual_journal_receiver_name           = %trimr( RRCV0100.Previous_dual_journal_receiver_name         );
      p_Previous_dual_journal_receiver_library_name   = %trimr( RRCV0100.Previous_dual_journal_receiver_library_name );
      p_Next_journal_receiver_name                    = %trimr( RRCV0100.Next_journal_receiver_name                  );
      p_Next_journal_receiver_library_name            = %trimr( RRCV0100.Next_journal_receiver_library_name          );
      p_Next_dual_journal_receiver_name               = %trimr( RRCV0100.Next_dual_journal_receiver_name             );
      p_Next_dual_journal_receiver_library_name       = %trimr( RRCV0100.Next_dual_journal_receiver_library_name     );
      p_Number_of_journal_entries_long                = %int(   RRCV0100.Number_of_journal_entries_long              );
      p_Maximum_entry_specific_data_length_long       = %int(   RRCV0100.Maximum_entry_specific_data_length_long     );
      p_First_sequence_number_long                    = %int(   RRCV0100.First_sequence_number_long                  );
      p_Last_sequence_number_long                     = %int(   RRCV0100.Last_sequence_number_long                   );
      p_ASP_device_name                               = %trimr( RRCV0100.ASP_device_name                             );
      p_Local_journal_ASP_group_name                  = %trimr( RRCV0100.Local_journal_ASP_group_name                );
      p_Source_journal_ASP_group_name                 = %trimr( RRCV0100.Source_journal_ASP_group_name               );

      select;
        when ( RRCV0100.Fixed_length_data_JOB = ' ' );
          n_Fixed_length_data_JOB = PARM_NULL;
        when ( RRCV0100.Fixed_length_data_JOB = '0' );
          p_Fixed_length_data_JOB = 'NO';
        when ( RRCV0100.Fixed_length_data_JOB = '1' );
          p_Fixed_length_data_JOB = 'YES';
      endsl;

      select;
        when ( RRCV0100.Fixed_length_data_USR = ' ' );
          n_Fixed_length_data_USR = PARM_NULL;
        when ( RRCV0100.Fixed_length_data_USR = '0' );
          p_Fixed_length_data_USR = 'NO';
        when ( RRCV0100.Fixed_length_data_USR = '1' );
          p_Fixed_length_data_USR = 'YES';
      endsl;

      select;
        when ( RRCV0100.Fixed_length_data_PGM = ' ' );
          n_Fixed_length_data_PGM = PARM_NULL;
        when ( RRCV0100.Fixed_length_data_PGM = '0' );
          p_Fixed_length_data_PGM = 'NO';
        when ( RRCV0100.Fixed_length_data_PGM = '1' );
          p_Fixed_length_data_PGM = 'YES';
      endsl;

      select;
        when ( RRCV0100.Fixed_length_data_PGMLIB = ' ' );
          n_Fixed_length_data_PGMLIB = PARM_NULL;
        when ( RRCV0100.Fixed_length_data_PGMLIB = '0' );
          p_Fixed_length_data_PGMLIB = 'NO';
        when ( RRCV0100.Fixed_length_data_PGMLIB = '1' );
          p_Fixed_length_data_PGMLIB = 'YES';
      endsl;

      select;
        when ( RRCV0100.Fixed_length_data_SYSSEQ = ' ' );
          n_Fixed_length_data_SYSSEQ = PARM_NULL;
        when ( RRCV0100.Fixed_length_data_SYSSEQ = '0' );
          p_Fixed_length_data_SYSSEQ = 'NO';
        when ( RRCV0100.Fixed_length_data_SYSSEQ = '1' );
          p_Fixed_length_data_SYSSEQ = 'YES';
      endsl;

      select;
        when ( RRCV0100.Fixed_length_data_RMTADR = ' ' );
          n_Fixed_length_data_RMTADR = PARM_NULL;
        when ( RRCV0100.Fixed_length_data_RMTADR = '0' );
          p_Fixed_length_data_RMTADR = 'NO';
        when ( RRCV0100.Fixed_length_data_RMTADR = '1' );
          p_Fixed_length_data_RMTADR = 'YES';
      endsl;

      select;
        when ( RRCV0100.Fixed_length_data_THD = ' ' );
          n_Fixed_length_data_THD = PARM_NULL;
        when ( RRCV0100.Fixed_length_data_THD = '0' );
          p_Fixed_length_data_THD = 'NO';
        when ( RRCV0100.Fixed_length_data_THD = '1' );
          p_Fixed_length_data_THD = 'YES';
      endsl;

      select;
        when ( RRCV0100.Fixed_length_data_LUW = ' ' );
          n_Fixed_length_data_LUW = PARM_NULL;
        when ( RRCV0100.Fixed_length_data_LUW = '0' );
          p_Fixed_length_data_LUW = 'NO';
        when ( RRCV0100.Fixed_length_data_LUW = '1' );
          p_Fixed_length_data_LUW = 'YES';
      endsl;

      select;
        when ( RRCV0100.Fixed_length_data_XID = ' ' );
          n_Fixed_length_data_XID = PARM_NULL;
        when ( RRCV0100.Fixed_length_data_XID = '0' );
          p_Fixed_length_data_XID = 'NO';
        when ( RRCV0100.Fixed_length_data_XID = '1' );
          p_Fixed_length_data_XID = 'YES';
      endsl;

      select;
        when ( RRCV0100.Journal_entries_filtered = ' ' );
          n_Journal_entries_filtered = PARM_NULL;
        when ( RRCV0100.Journal_entries_filtered = '0' );
          p_Journal_entries_filtered = 'NO';
        when ( RRCV0100.Journal_entries_filtered = '1' );
          p_Journal_entries_filtered = 'YES';
      endsl;

      p_Number_of_programs_to_filter                  = RRCV0100.Number_of_programs_to_filter                ;
      select;
        when ( RRCV0100.Number_of_programs_to_filter = -1 );
          n_Filter_by_program_array = PARM_NULL;
        when ( RRCV0100.Number_of_programs_to_filter = 0 );
          p_Filter_by_program_array                   = '';
        when ( RRCV0100.Number_of_programs_to_filter * 20 <= %size( RRCV0100.Filter_by_program_array ) );
          p_Filter_by_program_array                   = %subst( RRCV0100.Filter_by_program_array : 1 : 20 * RRCV0100.Number_of_programs_to_filter );
        other;  // More programs than could be returned...
          p_Filter_by_program_array                   = RRCV0100.Filter_by_program_array                     ;
      endsl;

      select;
        when ( RRCV0100.Filter_by_object = ' ' );
          n_Filter_by_object = PARM_NULL;
        when ( RRCV0100.Filter_by_object = '0' );
          p_Filter_by_object = 'NO';
        when ( RRCV0100.Filter_by_object = '1' );
          p_Filter_by_object = 'YES';
      endsl;

      select;
        when ( RRCV0100.Filter_images = ' ' );
          n_Filter_images = PARM_NULL;
        when ( RRCV0100.Filter_images = '0' );
          p_Filter_images = 'NO';
        when ( RRCV0100.Filter_images = '1' );
          p_Filter_images = 'YES';
      endsl;

  endsl;

  SaveCallType = CallType;
  SaveLoLib    = LoLib;
  SaveLoRcv    = LoRcv;

  return;

end-proc;

//----------------------------------------------------------------
// Procedure:    CvtTimestamp
// Description:  Convert CYYMMDDHHMMSS to ISO timestamp.

dcl-proc CvtTimestamp;

  dcl-pi *n timestamp;
    RcvTS   like( RRCV0100_t.Attached_date_and_time );
  end-pi;

  dcl-s  Result  timestamp inz( *loval );

  monitor;
    if ( RcvTS <> *blanks ) and
       ( RcvTS <> *zeros );
      Result = %date( %subst( RcvTS : 1 : 7 ) : *CYMD0 ) + %time( %subst( RcvTS : 8 ) : *HMS0 );
    endif;
  on-error *ALL;
  endmon;

  return ( Result );

end-proc;
